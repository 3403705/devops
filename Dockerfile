#See https://aka.ms/containerfastmode to understand how Visual Studio uses this Dockerfile to build your images for faster debugging.

FROM mcr.microsoft.com/dotnet/sdk:6.0 AS build
WORKDIR /app

COPY DevOPS/DevOPS.csproj ./
RUN dotnet restore 
RUN dotnet tool update --global dotnet-ef

ENV PATH="$PATH:/root/.dotnet/tools"

COPY /DevOPS/ ./
RUN dotnet publish -c Release -o out
RUN dotnet ef migrations add initial && dotnet-ef database update

FROM mcr.microsoft.com/dotnet/aspnet:6.0
WORKDIR /app
EXPOSE 80
COPY --from=build /app/out .
ENTRYPOINT ["dotnet", "DevOPS.dll"]